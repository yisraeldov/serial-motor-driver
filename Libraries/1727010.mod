PCBNEW-LibModule-V1  2020-02-13 11:09:01
# encoding utf-8
Units mm
$INDEX
SHDR2W90P0X350_1X2_750X730X850P
$EndINDEX
$MODULE SHDR2W90P0X350_1X2_750X730X850P
Po 0 0 0 15 5e452e4d 00000000 ~~
Li SHDR2W90P0X350_1X2_750X730X850P
Cd 1727010
Kw Connector
Sc 0
At STD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "J**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "SHDR2W90P0X350_1X2_750X730X850P"
DS -2.25 3.85 -2.25 -3.95 0.05 24
DS -2.25 -3.95 5.75 -3.95 0.05 24
DS 5.75 -3.95 5.75 3.85 0.05 24
DS 5.75 3.85 -2.25 3.85 0.05 24
DS -2 3.6 -2 -3.7 0.1 24
DS -2 -3.7 5.5 -3.7 0.1 24
DS 5.5 -3.7 5.5 3.6 0.1 24
DS 5.5 3.6 -2 3.6 0.1 24
DS 0 3.6 5.5 3.6 0.2 21
DS 5.5 3.6 5.5 -3.7 0.2 21
DS 5.5 -3.7 -2 -3.7 0.2 21
DS -2 -3.7 -2 0 0.2 21
$PAD
Po 0 0
Sh "1" R 2.175 2.175 0 0 900
Dr 1.45 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3.5 0
Sh "2" C 2.175 2.175 0 0 900
Dr 1.45 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE SHDR2W90P0X350_1X2_750X730X850P
$EndLIBRARY
