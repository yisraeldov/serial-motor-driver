This is a KiCad design of a PCB that takes a serial input and uses it to drive 
DC motors. It also is chainable and has 4 GPIO pins.

Using the <https://www.ti.com/lit/ds/symlink/drv8833.pdf> 
Take a look at the board from adafruit <https://learn.adafruit.com/adafruit-drv8833-dc-stepper-motor-driver-breakout-board/downloads>

I don't think we need the current limiting features
![3d View](serial-motor-driver.png)
![Top View](serial-motor-driver-top.png)
![Bottom View](serial-motor-driver-bot.png)
